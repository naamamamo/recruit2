<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('interviews')->insert([
        [
            'Date' => '2020-07-15',
            'summary' => 'Good Interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'Date' => '2020-07-15',
            'summary' => 'Bad Interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()   
        ],                  
        ]);         
}
}
